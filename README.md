Bài tập nhóm môn Mẫu thiết kế CTK45 - Nhóm 11 - Shop Online

Hướng dẫn sử dụng:
    
    + B1: Tải project về máy
    + B2: Giải nén vào một thư mục bất kì
    + B3: Mở SQL Server, kết nối vào server của máy, sau đó Attach file manfashion.mdf nằm trong thư mục Database để tạo database
    + B4: Mở project bằng Visual Studio --> chọn ShopOnline --> Chọn thư mục Model --> Click đôi vào file Model.edmx
    + B5: Update model cho project, sau đó Rebuild app
    + B6: Debug app và sử dụng

